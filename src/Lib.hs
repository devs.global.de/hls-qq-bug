{-# language QuasiQuotes #-}
module Lib where 

import Hasql.TH  -- from hasql-th

oneStatement =
      [resultlessStatement| 
      insert into a (c) values (0)
      |]